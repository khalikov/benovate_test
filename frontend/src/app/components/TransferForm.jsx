import React from 'react';
import UserField from './UserField';
import InnField from './InnField';
import SumField from './SumField';

export class TransferForm extends React.Component {
    render(){
        return (
            <form>
                <UserField />
                <InnField />
                <SumField />
            </form>
        )
    }
}