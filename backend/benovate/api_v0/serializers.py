from rest_framework import serializers
from users.models import User

class UserSerializer(serializers.ModelSerializer):
    
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        """
        Выдает `username` в качестве имени. Можно заменить на
        полное имя
        """
        return obj.username

    class Meta:
        fields = [
            'id',
            'name'
        ]
        model = User
