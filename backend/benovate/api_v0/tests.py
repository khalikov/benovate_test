from django.test import TestCase
import requests
DJANGO_SERVER = 'http://127.0.0.1:8000'
import json

class TestTransfer(TestCase):
    def test_transfer(self):
        response = requests.request(
            'POST',
            '%s/api/v0/transfer/' % DJANGO_SERVER,
            json=json.dumps({
                'user_id': 1,
                'inn_list': '222-222-222 22',
            }),
        )
        self.assertEqual(response.status_code, 200)
        # print(response.text)
    
    def test_users(self):
        response = requests.request(
            'GET',
            '%s/api/v0/user-list/' % DJANGO_SERVER,
            json=json.dumps({
                'query': 'dm'
            })
        )
        self.assertEqual(response.status_code, 200)
        try:
            j1 = json.loads(response.text)
        except ValueError:
            assert False, 'JSON not decodable'
        self.assertEqual(len(j1['results']), 1)