import decimal
import json
from django.shortcuts import render
from django.db.transaction import atomic
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from users.models import User
from . import serializers


class TransferException(Exception):
    pass

class UserViewSet(ModelViewSet):
    http_method_names = ['get']
    serializer_class = serializers.UserSerializer
    
    def get_queryset(self):
        try:
            query = json.loads(self.request.data)['query']
        except ValueError:
            raise HTTP_400_BAD_REQUEST
        except KeyError:
            raise HTTP_400_BAD_REQUEST

        return User.objects.filter(username__icontains=query)


class TransferAPIView(APIView):

    def make_tests(self, user_id, inn_list, transfer_sum):
        inn_list = str(inn_list).split(',')
        # Поскольку за время транзакции могут измениться счета
        # то необходимо блокировать строки
        user = User.objects.filter(id=user_id).select_for_update().last()
        if not user:
            raise TransferException('Не найден пользователь-плательщик')
        if user.inn in inn_list:
            raise TransferException('Невозможно передать сумму самому себе')
        if user.account < transfer_sum:
            raise TransferException('Недостаточно средств')
        receivers = User.objects.filter(inn__in=inn_list).select_for_update()
        r_count = receivers.count()
        if transfer_sum <= 0:
            raise TransferException('Сумма меньше или равна нуля')
        if r_count == 0:
            raise TransferException('Получатели не найдены')           
        single_sum = round(transfer_sum / r_count, 2)
        if single_sum < Decimal('0.01'):
            raise TransferException('Сумма на каждого получателя меньше 1 копейки')
        sum_to_pay = single_sum * r_count
        return sum_to_pay, single_sum, r_count, receivers, user
        
    @atomic
    def transfer(self, user_id, inn_list, transfer_sum):
        sum_to_pay, single_sum, r_count, receivers, user = self.make_tests(
            user_id, inn_list, transfer_sum)
        User.objects.filter(id=user.id).update(account=F('account') - transfer_sum)
        receivers.update(account=F('account') + single_sum)

    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.data)
        except ValueError:
            return Response({}, status=HTTP_400_BAD_REQUEST)
        user_id = data.get('user_id')
        inn_list = data.get('inn_list')
        transfer_sum = decimal.Decimal(str(data.get('transfer_sum')))
        try:
            self.transfer(user_id, inn_list, transfer_sum)
        except TransferError as exc:
            return Response({
                'message': exc.message
            }, status_code=HTTP_400_BAD_REQUEST)
        return Response({
            'message': 'Транзакция успешно проведена'
        })