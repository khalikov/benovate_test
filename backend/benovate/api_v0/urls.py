from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register('user-list', views.UserViewSet, 'user-list')

urlpatterns = router.urls + [
    url(r'transfer/$', views.TransferAPIView.as_view()),
]