from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):

    inn = models.CharField(
        'ИНН', db_index=True, help_text='В формате ###-###-### ##',
        max_length=14
    )
    account = models.DecimalField(
        'Счёт',
        max_digits=17, decimal_places=2, default=0
    )

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.username
